import { PlanetService } from "@eternal-twin/galaxy55-api-types/planet/service.js";
import { User } from "@eternal-twin/etwin-api-types/user/user.js";
import { UserId } from "@eternal-twin/etwin-api-types/user/user-id.js";
import { Api } from "@eternal-twin/etwin-api-types/api.js";

export class InMemoryPlanetService implements PlanetService {
  private readonly etwinApi: Api;
  private readonly userIds: Set<UserId>;

  public constructor(etwinApi: Api) {
    this.etwinApi = etwinApi;
    this.userIds = new Set();
  }

  public async getPlayersOnPlanet(_planetId: string): Promise<User[]> {
    const users: User[] = [];
    for (const userId of this.userIds) {
      const user: User | null = await this.etwinApi.getUserById("", "", userId);
      if (user !== null) {
        users.push(user);
      }
    }
    return users;
  }

  public addUser(userId: UserId): void {
    this.userIds.add(userId);
  }
}
