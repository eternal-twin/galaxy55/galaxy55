import * as assert from "assert";
import { InMemoryPlanetService } from "../../lib/planet/service.js";
import { InMemoryApi } from "@eternal-twin/etwin-api-in-memory/api.js";
import { User } from "@eternal-twin/etwin-api-types/user/user.js";

describe("InMemoryPlanetService", () => {
  it("compiles", async () => {
    const etwinApi: InMemoryApi = new InMemoryApi();
    const planet: InMemoryPlanetService = new InMemoryPlanetService(etwinApi);
    {
      const users = await planet.getPlayersOnPlanet("nalikors");
      assert.strictEqual(users.length, 0);
    }
    const alice: User = {id: "aaa-a-aa", displayName: "Alice"};
    etwinApi.addInMemoryUser(alice);
    planet.addUser(alice.id);
    {
      const users = await planet.getPlayersOnPlanet("nalikors");
      assert.deepStrictEqual(users, [alice]);
    }
  });
});
