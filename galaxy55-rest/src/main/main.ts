import Koa from "koa";
import koaLogger from "koa-logger";
import koaMount from "koa-mount";
import { createApiRouter } from "../lib/index.js";
import { InMemoryPlanetService } from "@eternal-twin/galaxy55-api-in-memory/planet/service.js";
import { InMemoryApi } from "@eternal-twin/etwin-api-in-memory/api.js";
import { User } from "@eternal-twin/etwin-api-types/user/user.js";

async function main(): Promise<void> {
  const etwin = new InMemoryApi();
  const planet = new InMemoryPlanetService(etwin);
  const apiRouter = createApiRouter({planet});

  const alice: User = {id: "aaa-a-aa", displayName: "Alice"};
  etwin.addInMemoryUser(alice);
  planet.addUser(alice.id);

  const app: Koa = new Koa();
  const port: number = 50320;

  app.use(koaLogger());
  app.use(koaMount("/", apiRouter));

  app.listen(port, () => {
    console.log(`Listening on http://localhost:${port}`);
  });
}

main()
  .catch((err: Error): never => {
    console.error(err.stack);
    process.exit(1);
  });
